import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { GlobalColors } from '../../globals';
import Text from '../Text';
// import SectionHeadline from '../UiElements/SectionHeadline';
// import TransactionsTable from './TransactionTable';

const useStyles = makeStyles(() => ({
  wrapper: {
    maxWidth: 72,
    padding: '8px',
    textAlign: 'center',
    borderRadius: 6,
  },
}));

interface Props {
  label: string;
  fontSize?: number;
  fontWeight?: number;
  color?: GlobalColors;
  backgroundColor?: GlobalColors;
}

const Label: React.FC<Props> = ({ label, color, backgroundColor }) => {
  const { wrapper } = useStyles();
  const inlineStyles = {
    // fontSize,
    // fontWeight,
    // color,
    backgroundColor,
  };

  return (
    <div className={wrapper} style={inlineStyles}>
      <Text label={label} fontSize={15} color={color} />
    </div>
  );
};

export default Label;
