import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import FilterArrowIcon from '../../assets/icons/filter-arrow.svg';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      display: 'block',
      marginTop: theme.spacing(2),
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectRoot: {
      fontFamily: 'Jost',
      color: '#808791',
      fontSize: 15,
    },
    select: {
      backgroundColor: 'transparent !important',
    },
    labelRoot: {
      fontFamily: 'Jost',
      color: '#808791!important',
      fontSize: 15,
    },
    menuItem: {
      fontFamily: 'Jost',
      color: '#808791',
      fontSize: 15,
    },
  })
);

interface FilterSelectProps {
  label: string;
  values: string[];
}

const FilterSelect: React.FC<FilterSelectProps> = ({ label, values }) => {
  const classes = useStyles();
  const [value, setValue] = React.useState<string>('');
  const [open, setOpen] = React.useState(false);

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>): void => {
    setValue(event.target.value as string);
  };

  const handleClose = (): void => {
    setOpen(false);
  };

  const handleOpen = (): void => {
    setOpen(true);
  };

  return (
    <div>
      <FormControl className={classes.formControl}>
        <InputLabel
          id="demo-controlled-open-select-label"
          classes={{
            root: classes.labelRoot,
          }}
        >
          {label}
        </InputLabel>
        <Select
          labelId="demo-controlled-open-select-label"
          id="demo-controlled-open-select"
          open={open}
          onClose={handleClose}
          onOpen={handleOpen}
          value={value}
          onChange={handleChange}
          disableUnderline
          /* eslint-disable-next-line @typescript-eslint/explicit-function-return-type */
          IconComponent={(props) => (
            <i {...props} className={`material-icons ${props.className}`}>
              <img src={FilterArrowIcon} alt="arrow" />
            </i>
          )}
          classes={{
            root: classes.selectRoot,
            select: classes.select,
          }}
        >
          {values.map((val) => (
            <MenuItem value={val} className={classes.menuItem}>
              {val}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
};

export default FilterSelect;
