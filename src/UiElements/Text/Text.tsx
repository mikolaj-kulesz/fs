import React, { CSSProperties } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { GlobalColors } from '../../globals';

const useStyles = makeStyles(() => ({
  wrapper: {
    // maxWidth: 1096,
    // background: 'white',
    // margin: '0 auto',
    // borderRadius: 10,
    // padding: '30px 20px',
  },
}));

interface Props {
  label: string;
  fontSize?: number;
  fontWeight?: number;
  color?: GlobalColors;
  styles?: CSSProperties;
}

const Text: React.FC<Props> = ({
  label,
  fontSize,
  fontWeight,
  color,
  styles,
}) => {
  const { wrapper } = useStyles();
  const inlineStyles = {
    fontSize,
    fontWeight,
    color,
    ...styles,
  };

  return (
    <p className={wrapper} style={inlineStyles}>
      {label}
    </p>
  );
};

export default Text;
