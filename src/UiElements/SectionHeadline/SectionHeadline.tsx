import React, { CSSProperties } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { SectionHeadlineSizes } from '../../globals';

const useStyles = makeStyles(() => ({
  headline: {
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: 18,
    lineHeight: 1.2,
    letterSpacing: '0.5px',
  },
}));

interface Props {
  label: string;
  size?: SectionHeadlineSizes;
  styles?: CSSProperties;
}

const SectionHeadline: React.FC<Props> = ({ label, size, styles }) => {
  const { headline } = useStyles();
  const style = {
    fontSize: size,
    ...styles,
  };

  return (
    <h2 className={headline} style={style}>
      {label}
    </h2>
  );
};

export default SectionHeadline;
