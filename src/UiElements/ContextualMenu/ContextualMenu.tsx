import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { RouteComponentProps } from 'react-router';
import { withRouter } from 'react-router-dom';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import MoreIcon from '../../assets/icons/more-icon.svg';
import GlobalRoutes from '../../routes';

const useStyles = makeStyles(() =>
  createStyles({
    menuItem: {
      fontFamily: 'Jost',
      color: '#808791',
      fontSize: 15,
    },
  })
);

const options = [
  'Change category',
  'Change sth else',
  'Change sth else',
  'Change sth else',
  'Change sth else',
];

interface ContextualMenuProps extends RouteComponentProps {
  id: number;
}

export const ContextualMenu: React.FC<ContextualMenuProps> = ({
  id,
  history,
}) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLElement>): void => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const handleSelect = (): void => {
    history.replace(`/${GlobalRoutes.CHANGE_CATEGORY}/${id}`);
  };

  return (
    <div style={{ marginRight: '-20px' }}>
      <IconButton
        aria-label="more"
        aria-controls="long-menu"
        aria-haspopup="true"
        onClick={handleClick}
        size="small"
      >
        <div
          style={{
            width: 24,
            height: 24,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <img src={MoreIcon} alt="Click more" />
        </div>
      </IconButton>
      <Menu
        id="long-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
      >
        {options.map((option, index) => (
          <MenuItem
            className={classes.menuItem}
            key={option}
            disabled={index > 0}
            onClick={handleSelect}
          >
            {option}
          </MenuItem>
        ))}
      </Menu>
    </div>
  );
};

export default withRouter(ContextualMenu);
