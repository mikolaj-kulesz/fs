import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Text from '../Text';
import SortButtonDownIcon from './SortButtonDownIcon';
import SortButtonUpIcon from './SortButtonUpIcon';

const useStyles = makeStyles(() => ({
  sortElement: {
    display: 'flex',
    alignItems: 'center',
  },
  iconWrapper: {
    display: 'flex',
    width: 12,
    height: 12,
    alignItems: 'center',
    justifyContent: 'center',
    background: 'transparent',
    border: 'none',
    outline: 'none',
    cursor: 'pointer',
  },
}));

enum SortButtonIcon {
  UP = 'up',
  DOWN = 'down',
}

interface SortButtonProps {
  label: string;
}

const SortButton: React.FC<SortButtonProps> = ({ label }) => {
  const { sortElement, iconWrapper } = useStyles();
  const [downIconFillColor, setDownIconSetFillColor] = useState('#C4C4C4');
  const [upIconFillColor, setUpIconSetFillColor] = useState('#C4C4C4');
  const downIconClickHandler = (iconType: SortButtonIcon): void => {
    if (iconType === SortButtonIcon.UP) {
      setUpIconSetFillColor('#7038A7');
      setDownIconSetFillColor('#C4C4C4');
      return;
    }
    setUpIconSetFillColor('#C4C4C4');
    setDownIconSetFillColor('#7038A7');
  };

  return (
    <div className={sortElement}>
      <Text
        label={label}
        fontSize={12}
        styles={{
          fontFamily: `'Jost', sans-serif`,
          fontWeight: 600,
          textTransform: 'uppercase',
          color: '#BFBFBF',
          marginRight: 4,
        }}
      />
      <button
        type="button"
        className={iconWrapper}
        onClick={(): void => downIconClickHandler(SortButtonIcon.DOWN)}
      >
        <SortButtonDownIcon fillColor={downIconFillColor} />
      </button>
      <button
        type="button"
        className={iconWrapper}
        onClick={(): void => downIconClickHandler(SortButtonIcon.UP)}
      >
        <SortButtonUpIcon fillColor={upIconFillColor} />
      </button>
    </div>
  );
};

export default SortButton;
