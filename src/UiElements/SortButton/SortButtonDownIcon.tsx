import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  iconWrapper: {
    display: 'flex',
    width: 12,
    height: 12,
    alignItems: 'center',
    justifyContent: 'center',
  },
}));

interface SortButtonDownIconProps {
  fillColor: string;
}

const SortButtonDownIcon: React.FC<SortButtonDownIconProps> = ({
  fillColor,
}) => {
  const { iconWrapper } = useStyles();

  return (
    <div className={iconWrapper}>
      <svg
        width="8"
        height="5"
        viewBox="0 0 8 5"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path d="M4 5L0.535899 0.5L7.4641 0.5L4 5Z" fill={fillColor} />
      </svg>
    </div>
  );
};

export default SortButtonDownIcon;
