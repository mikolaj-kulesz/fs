import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

interface SortButtonUpIconProps {
  fillColor: string;
}

const useStyles = makeStyles(() => ({
  iconWrapper: {
    display: 'flex',
    width: 12,
    height: 12,
    alignItems: 'center',
    justifyContent: 'center',
  },
}));

const SortButtonUpIcon: React.FC<SortButtonUpIconProps> = ({ fillColor }) => {
  const { iconWrapper } = useStyles();

  return (
    <div className={iconWrapper}>
      <svg
        width="8"
        height="5"
        viewBox="0 0 8 5"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M4 6.99382e-07L7.4641 4.5L0.535898 4.5L4 6.99382e-07Z"
          fill={fillColor}
        />
      </svg>
    </div>
  );
};

export default SortButtonUpIcon;
