import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: '3px 10px',
      display: 'flex',
      alignItems: 'center',
      width: 220,
      border: '1px solid #E3E8EE',
      boxSizing: 'border-box',
      borderRadius: 8,
      boxShadow: 'none',
      marginTop: 10,
      marginRight: 20,
      [theme.breakpoints.down('sm')]: {
        width: '100%',
        marginTop: 16,
        marginRight: 0,
      },
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
      fontFamily: 'Jost',
      fontSize: 15,
    },
    iconButton: {
      padding: 10,
    },
    divider: {
      height: 28,
      margin: 4,
    },
  })
);
const SearchInput: React.FC = () => {
  const classes = useStyles();

  return (
    <Paper component="form" className={classes.root}>
      <SearchIcon />
      <InputBase
        className={classes.input}
        placeholder="Search"
        inputProps={{ 'aria-label': 'search google maps' }}
      />
    </Paper>
  );
};

export default SearchInput;
