export enum GlobalColors {
  PRIMARY = '#11C0AE',
  LIGHT_GRAY = '#EDEDED',
  GRAY = '#7F7F7F',
  PURPLE_LIGHT = '#FAFAFF',
  BLACK = '#000',
  WHITE = '#FFF',
  BACKDROP = 'rgba(41, 27, 55, 0.9)',
}

export enum SectionHeadlineSizes {
  LARGE = 22,
  MEDIUM = 18,
  SMALL = 14,
}
