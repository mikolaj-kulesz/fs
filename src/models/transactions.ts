import {
  types,
  SnapshotIn,
  applySnapshot,
  getSnapshot,
  Instance,
} from 'mobx-state-tree';

export interface Person {
  id: number;
  name: string;
  creditScore: number;
  email: string;
}

export interface Transaction {
  id: number;
  personId: number;
  amount: number;
  name: string;
  categoryName: string;
}

const Transaction = types.model({
  id: types.number,
  personId: types.number,
  amount: types.number,
  name: types.string,
  categoryName: types.string,
});

const Transactions = types
  .model({
    transactions: types.optional(types.array(Transaction), []),
  })
  .actions((self) => ({
    setTransactions(transactions: Transaction[]): void {
      const currentSnapshot: TransactionsSnapshot = getSnapshot(self);
      const newSnapshot = {
        ...currentSnapshot,
        transactions,
      };
      applySnapshot(self, newSnapshot);
    },
  }))
  .views((self) => ({
    getTransactions(): Transaction[] {
      const { transactions } = getSnapshot(self);
      return [...transactions].reverse();
    },
  }));

export const transactions = Transactions.create();

export type TransactionsSnapshot = SnapshotIn<typeof Transactions>;
export type TransactionsInstance = Instance<typeof Transactions>;
