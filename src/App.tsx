import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Route } from 'react-router-dom';
import Transactions from './Transactions';
import ActionModal from './ActionModal/ActionModal';
import GlobalRoutes from './routes';
import TransactionsContextProvider from './contexts/Transactions';

const useStyles = makeStyles(() => ({
  wrapper: {
    background: '#F5F5FB',
  },
}));

interface Person {
  id: number;
  name: string;
  creditScore: number;
  email: string;
}

interface Transaction {
  id: number;
  personId: number;
  amount: number;
  name: string;
  categoryName: string;
}

const App: React.FC = () => {
  const { wrapper } = useStyles();
  return (
    <TransactionsContextProvider>
      <div className={wrapper}>
        <Transactions />
        <Route
          path={`/${GlobalRoutes.CHANGE_CATEGORY}/:id`}
          component={ActionModal}
        />
      </div>
    </TransactionsContextProvider>
  );
};

export default App;
