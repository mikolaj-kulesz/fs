import React, { useContext, useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import socketIOClient from 'socket.io-client';
import {
  TransactionsInstance,
  transactions,
  Transaction,
  Person,
} from '../models/transactions';

interface TransactionsContextInterface {
  transactions: TransactionsInstance;
  category: string | null;
  setCategory: React.Dispatch<React.SetStateAction<string | null>>;
}

export const TransactionsContext = React.createContext<
  TransactionsContextInterface
>({
  transactions,
  category: null,
  setCategory: () => null,
});

const ENDPOINT = 'ws://localhost:5000';
export const socket = socketIOClient(ENDPOINT);

const TransactionsContextProvider: React.FC = ({ children }) => {
  const [transaction, setTransaction] = useState<Transaction>();
  const [category, setCategory] = useState<string | null>(null);

  useEffect(() => {
    socket.on(
      'personTransaction',
      (newPerson: Person, newTransaction: Transaction) => {
        setTransaction(newTransaction);
      }
    );

    socket.on('updateTransactions', (data: Transaction[]) => {
      transactions.setTransactions(data);
    });

    socket.emit('subscribeToServer');
  }, []);

  useEffect(() => {
    if (transaction) {
      socket.emit('updateTransaction', transaction);
    }
  }, [transaction]);

  const value = {
    transactions,
    category,
    setCategory,
  };

  return (
    <TransactionsContext.Provider value={value}>
      {children}
    </TransactionsContext.Provider>
  );
};

export const useTransactionsContext = (): TransactionsContextInterface => {
  const transactionsContext = useContext(TransactionsContext);

  if (transactionsContext === null) {
    throw new Error(
      'transactionsContext must be used within the ThemeEditorContextProvider!'
    );
  }

  return {
    ...transactionsContext,
  };
};

export default observer(TransactionsContextProvider);
