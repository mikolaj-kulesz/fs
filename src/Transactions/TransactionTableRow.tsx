import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import cn from 'classnames';
import Text from '../UiElements/Text';
import Label from '../UiElements/Label';
import ContextualMenu from '../UiElements/ContextualMenu';
import { GlobalColors } from '../globals';
import { Transaction } from '../models/transactions';

const useStyles = makeStyles(() => ({
  wrapper: {
    borderBottom: `1px solid ${GlobalColors.LIGHT_GRAY}`,
    transition: 'background 1s ease-in-out',
  },
  wrapperSpecial: {
    background: GlobalColors.PRIMARY,
  },
  row: {
    padding: '30px 0',
  },
  paper: {},
}));

interface TransactionsTableRowProps {
  transaction: Transaction;
}

const TransactionsTableRow: React.FC<TransactionsTableRowProps> = ({
  transaction,
}) => {
  const { wrapper, wrapperSpecial, row } = useStyles();
  const [isSpecial, setIsSpecial] = useState(true);
  const { id, name, categoryName, amount } = transaction;

  const displayCategoryName = categoryName.replace('_', ' ');
  const expend = amount <= 30 && amount;
  const income = amount > 30 && amount;

  const timeout = (ms: number): Promise<void> => {
    return new Promise((resolve) => setTimeout(resolve, ms));
  };
  const removeSpecialStyling = async (): Promise<void> => {
    await timeout(100);
    setIsSpecial(false);
  };

  useEffect(() => {
    removeSpecialStyling().then();
  });

  return (
    <div className={cn(wrapper, { [wrapperSpecial]: isSpecial })}>
      <Grid
        container
        alignItems="center"
        item
        xs={12}
        spacing={1}
        className={row}
      >
        <Grid item xs={2}>
          <Text label="9 jun 2020" fontSize={14} />
        </Grid>
        <Grid item xs={2}>
          <Text label={name} fontSize={14} fontWeight={600} />
          <Text label="lorem ipsum dolor..." fontSize={12} />
        </Grid>
        <Grid item xs={2}>
          <Text
            label={displayCategoryName}
            fontSize={14}
            styles={{
              textTransform: 'capitalize',
            }}
          />
        </Grid>
        <Grid item xs={2}>
          <Label
            label="Fixed"
            fontSize={14}
            backgroundColor={GlobalColors.LIGHT_GRAY}
          />
        </Grid>
        <Grid container alignItems="center" item xs={4} spacing={1}>
          <Grid item xs={4}>
            {income && <Text label={`£${income}`} fontSize={15} />}
          </Grid>
          <Grid item xs={4}>
            {expend && <Text label={`-£${expend}`} fontSize={15} />}
          </Grid>
          <Grid item xs={4}>
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
            >
              <Text label="£120" fontSize={15} color={GlobalColors.PRIMARY} />
              <ContextualMenu id={id} />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

export default TransactionsTableRow;
