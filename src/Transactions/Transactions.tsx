import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { observer } from 'mobx-react';
import SectionHeadline from '../UiElements/SectionHeadline';
import FilterSelect from '../UiElements/FilterSelect';
import TransactionTableRow from './TransactionTableRow';
import TransactionsTableHeadRow from './TransactionTableHeadRow';
import Text from '../UiElements/Text';
import SearchInput from '../UiElements/SearchInput';
import { useTransactionsContext } from '../contexts/Transactions';

const useStyles = makeStyles(() => ({
  wrapper: {
    maxWidth: 1096,
    background: 'white',
    margin: '0 auto',
    borderRadius: 10,
    padding: '30px 20px',
  },
  filterWrapper: {
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  divider: {
    height: 40,
    width: 1,
    backgroundColor: '#E3E8EE',
    marginTop: 10,
  },
}));

const Transactions: React.FC = () => {
  const { wrapper, filterWrapper, divider } = useStyles();
  const { transactions } = useTransactionsContext();
  const transactionList = transactions.getTransactions();

  return (
    <div className={wrapper}>
      <pre>{JSON.stringify(transactions.getTransactions)}</pre>
      <SectionHeadline label="Transactions" />
      <div className={filterWrapper}>
        <SearchInput />
        <Text
          label="filter:"
          fontSize={12}
          styles={{
            fontFamily: `'Jost', sans-serif`,
            fontWeight: 600,
            textTransform: 'uppercase',
            color: '#BFBFBF',
            marginRight: 4,
            marginTop: 16,
          }}
        />
        <FilterSelect label="Type" values={['Fixed', 'Flexible']} />
        <div className={divider} />
        <FilterSelect
          label="Category"
          values={['Category Name 1', 'Category Name 2']}
        />
        <div className={divider} />
        <FilterSelect label="Date" values={['Data a', 'Data b']} />
        <div className={divider} />
        <FilterSelect label="In/Out" values={['In', 'Out']} />
        <div className={divider} />
      </div>

      <TransactionsTableHeadRow />
      {transactionList.map((transaction) => (
        <TransactionTableRow
          key={`${transaction.id}-${transaction.personId}`}
          transaction={transaction}
        />
      ))}
    </div>
  );
};

export default observer(Transactions);
