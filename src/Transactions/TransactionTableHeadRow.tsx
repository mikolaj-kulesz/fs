import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import SortButton from '../UiElements/SortButton';

const useStyles = makeStyles(() => ({
  row: {
    padding: '12px 0',
  },
  paper: {},
  bottomLine: {
    height: 5,
    backgroundColor: '#F5F5FB',
    borderRadius: 5,
    border: 'none',
  },
}));

const TransactionsTableHeadRow: React.FC = () => {
  const { row, bottomLine } = useStyles();

  return (
    <div>
      <Grid
        container
        alignItems="center"
        item
        xs={12}
        spacing={1}
        className={row}
      >
        <Grid item xs={2}>
          <SortButton label="Date" />
        </Grid>
        <Grid item xs={2}>
          <SortButton label="Description" />
        </Grid>
        <Grid item xs={2}>
          <SortButton label="Category" />
        </Grid>
        <Grid item xs={2}>
          <SortButton label="Type" />
        </Grid>
        <Grid container alignItems="center" item xs={4} spacing={1}>
          <Grid item xs={4}>
            <SortButton label="Income" />
          </Grid>
          <Grid item xs={4}>
            <SortButton label="Expend" />
          </Grid>
          <Grid item xs={4}>
            <SortButton label="Balance" />
          </Grid>
        </Grid>
      </Grid>
      <hr className={bottomLine} />
    </div>
  );
};

export default TransactionsTableHeadRow;
