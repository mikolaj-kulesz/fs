import React from 'react';
import { Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import { RouteComponentProps } from 'react-router';
import { GlobalColors } from '../globals';
import { useTransactionsContext, socket } from '../contexts/Transactions';

interface ActionModalButtonSaveProps extends RouteComponentProps {
  id: string;
}

const ActionModalButtonSave: React.FC<ActionModalButtonSaveProps> = ({
  id,
  history,
}) => {
  const { category, transactions } = useTransactionsContext();
  const SaveButton = withStyles(() => ({
    root: {
      color: GlobalColors.WHITE,
      fontFamily: `'Jost', sans-serif`,
      fontSize: 18,
      fontWeight: 600,
      backgroundColor: '#7038A7',
      padding: '10px 60px',
      textTransform: 'capitalize',
      borderRadius: 8,
      '&:hover': {
        backgroundColor: '#653297',
      },
    },
  }))(Button);

  const clickHandler = (): void => {
    const transactionList = transactions.getTransactions();
    const currentTransaction = transactionList.find(
      (t) => t.id === parseInt(id, 10)
    );

    history.replace('/');

    if (!currentTransaction || !category) return;

    socket.emit('updateTransaction', {
      ...currentTransaction,
      categoryName: category,
    });
  };

  return <SaveButton onClick={clickHandler}>Save</SaveButton>;
};

export default withRouter(ActionModalButtonSave);
