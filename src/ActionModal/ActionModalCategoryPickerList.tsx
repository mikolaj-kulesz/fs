import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ActionModalCategoryPickerHeadline from './ActionModalCategoryPickerHeadline';
import ActionModalCategoryPickerListItem, {
  ActionModalCategoryPickerListItemProps,
} from './ActionModalCategoryPickerListItem';

const useStyles = makeStyles(() => ({
  list: {
    maxHeight: 200,
    overflow: 'auto',
    paddingRight: 20,
  },
}));

interface ActionModalCategoryPickerListProps {
  listItems: ActionModalCategoryPickerListItemProps[];
  label: string;
}

const ActionModalCategoryPickerList: React.FC<ActionModalCategoryPickerListProps> = ({
  listItems,
  label,
}) => {
  const { list } = useStyles();
  return (
    <>
      <ActionModalCategoryPickerHeadline label={label} />
      <List className={list}>
        {listItems.map(
          ({
            iconComponent,
            iconComponentAlt,
            label: listItemLabel,
            iconStyle,
          }) => (
            <ActionModalCategoryPickerListItem
              iconComponent={iconComponent}
              iconComponentAlt={iconComponentAlt}
              label={listItemLabel}
              iconStyle={{ ...iconStyle }}
            />
          )
        )}
      </List>
    </>
  );
};

export default ActionModalCategoryPickerList;
