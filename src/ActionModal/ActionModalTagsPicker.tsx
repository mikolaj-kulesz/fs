import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { GlobalColors } from '../globals';
import Text from '../UiElements/Text';

const useStyles = makeStyles(() => ({
  paper: {
    background: GlobalColors.PURPLE_LIGHT,
    borderRadius: 8,
    padding: '14px 28px',
  },
}));

const ActionModalTagsPicker: React.FC = () => {
  const { paper } = useStyles();

  // const listItems = [
  //   {
  //     iconComponent: CategoryIcon1,
  //     iconComponentAlt: `Category 1`,
  //     label: 'Inbox',
  //   },
  //   {
  //     iconComponent: CategoryIcon2,
  //     iconComponentAlt: `Category 2`,
  //     label: 'Inbox',
  //     iconStyle: {
  //       marginLeft: 4,
  //     },
  //   },
  //   {
  //     iconComponent: CategoryIcon3,
  //     iconComponentAlt: `Category 3`,
  //     label: 'Inbox',
  //   },
  //   {
  //     iconComponent: CategoryIcon1,
  //     iconComponentAlt: `Category 1`,
  //     label: 'Inbox',
  //   },
  //   {
  //     iconComponent: CategoryIcon2,
  //     iconComponentAlt: `Category 2`,
  //     label: 'Inbox',
  //     iconStyle: {
  //       marginLeft: 4,
  //     },
  //   },
  //   {
  //     iconComponent: CategoryIcon3,
  //     iconComponentAlt: `Category 3`,
  //     label: 'Inbox',
  //   },
  // ];

  return (
    <div className={paper}>
      <Grid container alignItems="flex-start">
        <div>
          <Text
            label="Green"
            fontSize={24}
            styles={{
              color: '#7038A7',
              lineHeight: '35px',
              backgroundColor: '#EFEFF9',
              padding: '0 8px',
              borderRadius: 6,
              margin: '0 10px 10px 0',
              cursor: 'pointer',
            }}
          />
        </div>
        <div>
          <Text
            label="Green"
            fontSize={24}
            styles={{
              color: '#9B9B9B',
              lineHeight: '35px',
              backgroundColor: '#EDEDED',
              padding: '0 8px',
              borderRadius: 6,
              margin: '0 10px 10px 0',
              cursor: 'pointer',
            }}
          />
        </div>
        <div>
          <Text
            label="Green"
            fontSize={24}
            styles={{
              color: '#7038A7',
              lineHeight: '35px',
              backgroundColor: '#EFEFF9',
              padding: '0 8px',
              borderRadius: 6,
              margin: '0 10px 10px 0',
              cursor: 'pointer',
            }}
          />
        </div>
        <div>
          <Text
            label="Green"
            fontSize={24}
            styles={{
              color: '#9B9B9B',
              lineHeight: '35px',
              backgroundColor: '#EDEDED',
              padding: '0 8px',
              borderRadius: 6,
              margin: '0 10px 10px 0',
              cursor: 'pointer',
            }}
          />
        </div>
        <div>
          <Text
            label="Green"
            fontSize={24}
            styles={{
              color: '#7038A7',
              lineHeight: '35px',
              backgroundColor: '#EFEFF9',
              padding: '0 8px',
              borderRadius: 6,
              margin: '0 10px 10px 0',
              cursor: 'pointer',
            }}
          />
        </div>
        <div>
          <Text
            label="Green"
            fontSize={24}
            styles={{
              color: '#9B9B9B',
              lineHeight: '35px',
              backgroundColor: '#EDEDED',
              padding: '0 8px',
              borderRadius: 6,
              margin: '0 10px 10px 0',
              cursor: 'pointer',
            }}
          />
        </div>
        <div>
          <Text
            label="Green"
            fontSize={24}
            styles={{
              color: '#7038A7',
              lineHeight: '35px',
              backgroundColor: '#EFEFF9',
              padding: '0 8px',
              borderRadius: 6,
              margin: '0 10px 10px 0',
              cursor: 'pointer',
            }}
          />
        </div>
        <div>
          <Text
            label="Green"
            fontSize={24}
            styles={{
              color: '#9B9B9B',
              lineHeight: '35px',
              backgroundColor: '#EDEDED',
              padding: '0 8px',
              borderRadius: 6,
              margin: '0 10px 10px 0',
              cursor: 'pointer',
            }}
          />
        </div>
        <div>
          <Text
            label="Green"
            fontSize={24}
            styles={{
              color: '#7038A7',
              lineHeight: '35px',
              backgroundColor: '#EFEFF9',
              padding: '0 8px',
              borderRadius: 6,
              margin: '0 10px 10px 0',
              cursor: 'pointer',
            }}
          />
        </div>
        <div>
          <Text
            label="Green"
            fontSize={24}
            styles={{
              color: '#9B9B9B',
              lineHeight: '35px',
              backgroundColor: '#EDEDED',
              padding: '0 8px',
              borderRadius: 6,
              margin: '0 10px 10px 0',
              cursor: 'pointer',
            }}
          />
        </div>
      </Grid>
    </div>
  );
};

export default ActionModalTagsPicker;
