import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { GlobalColors } from '../globals';
import CategoryIcon1 from '../assets/icons/category-1.svg';
import CategoryIcon2 from '../assets/icons/category-2.svg';
import CategoryIcon3 from '../assets/icons/category-3.svg';
import ActionModalCategoryPickerList from './ActionModalCategoryPickerList';

const useStyles = makeStyles(() => ({
  paper: {
    background: GlobalColors.PURPLE_LIGHT,
    borderRadius: 8,
    padding: 28,
  },
}));

const ActionModalCategoryPicker: React.FC = () => {
  const { paper } = useStyles();

  const listItems = [
    {
      iconComponent: CategoryIcon1,
      iconComponentAlt: `Category ABC`,
      label: 'Category ABC',
    },
    {
      iconComponent: CategoryIcon2,
      iconComponentAlt: `Category DEF`,
      label: 'Category DEF',
      iconStyle: {
        marginLeft: 4,
      },
    },
    {
      iconComponent: CategoryIcon3,
      iconComponentAlt: `Category GHI`,
      label: 'Category GHI',
    },
    {
      iconComponent: CategoryIcon1,
      iconComponentAlt: `Category JKL`,
      label: 'Category JKL',
    },
    {
      iconComponent: CategoryIcon2,
      iconComponentAlt: `Category MN`,
      label: 'Category MN',
      iconStyle: {
        marginLeft: 4,
      },
    },
    {
      iconComponent: CategoryIcon3,
      iconComponentAlt: `Category PRS`,
      label: 'Category PRS',
    },
  ];

  return (
    <div className={paper}>
      <Grid container alignItems="flex-start" item xs={12} spacing={2}>
        <Grid item xs={6}>
          <ActionModalCategoryPickerList
            label="main category"
            listItems={listItems}
          />
        </Grid>
        <Grid item xs={6}>
          <ActionModalCategoryPickerList
            label="subcategory"
            listItems={listItems}
          />
        </Grid>
      </Grid>
    </div>
  );
};

export default ActionModalCategoryPicker;
