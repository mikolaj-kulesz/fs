import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Fade from '@material-ui/core/Fade';
import { RouteComponentProps } from 'react-router';
import Backdrop from '@material-ui/core/Backdrop';
import { GlobalColors, SectionHeadlineSizes } from '../globals';
import SectionHeadline from '../UiElements/SectionHeadline';
import Text from '../UiElements/Text';
import ActionModalCategoryPicker from './ActionModalCategoryPicker';
import ActionModalTagsPicker from './ActionModalTagsPicker';
import ActionModalButtons from './ActionModalButtons';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: 14,
    maxWidth: 715,
    width: '100%',
    border: 0,
    outline: 0,
    borderRadius: 10,
  },
  backdrop: {
    background: GlobalColors.BACKDROP,
  },
  bottomSpacing: {
    marginBottom: 28,
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  margin: {
    marginLeft: theme.spacing(1),
  },
  content: {
    maxHeight: '90vh',
    overflow: 'auto',
    padding: 14,
  },
}));

const ActionModal: React.FC<RouteComponentProps<{ id: string }>> = ({
  history,
  match,
}) => {
  const { backdrop, paper, modal, content } = useStyles();

  const handleClose = (): void => {
    history.replace('/');
  };
  const open = true;

  useEffect(() => {
    const { id } = match.params;
    if (!id) history.replace('/');
  }, [history, match]);

  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={modal}
      open={open}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        classes: {
          root: backdrop,
        },
      }}
    >
      <Fade in={open}>
        <div className={paper}>
          <div className={content}>
            <SectionHeadline
              label="Change Category"
              size={SectionHeadlineSizes.LARGE}
              styles={{
                marginBottom: 6,
              }}
            />
            <Text
              label="Help us classify better!"
              fontSize={16}
              color={GlobalColors.GRAY}
              styles={{
                marginBottom: 28,
              }}
            />
            <SectionHeadline
              label="Select a new category and subcategory"
              styles={{
                marginBottom: 28,
              }}
            />
            <ActionModalCategoryPicker />
            <SectionHeadline
              label="Select words in the transaction label that are defining the new category"
              styles={{
                marginBottom: 28,
                marginTop: 28,
              }}
            />
            <ActionModalTagsPicker />
            <ActionModalButtons id={match.params.id} />
          </div>
        </div>
      </Fade>
    </Modal>
  );
};

export default ActionModal;
