import React from 'react';
import Text from '../UiElements/Text';

interface ActionModalCategoryPickerHeadlineProps {
  label: string;
}

const ActionModalCategoryPickerHeadline: React.FC<ActionModalCategoryPickerHeadlineProps> = ({
  label,
}) => {
  return (
    <Text
      label={label}
      fontSize={12}
      fontWeight={600}
      styles={{
        textTransform: 'uppercase',
        color: '#BFBFBF',
        marginBottom: 36,
      }}
    />
  );
};

export default ActionModalCategoryPickerHeadline;
