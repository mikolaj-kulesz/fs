import React, { CSSProperties, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { useTransactionsContext } from '../contexts/Transactions';

const useStyles = makeStyles(() => ({
  listItem: {
    borderRadius: 4,
    '&:hover': {
      backgroundColor: '#EFEFF9',
    },
  },
  listItemText: {
    fontFamily: `'Jost', sans-serif`,
    fontSize: 14,
  },
  listIcon: {
    minWidth: 40,
  },
  selected: {
    backgroundColor: '#EFEFF9 !important',
  },
}));

export interface ActionModalCategoryPickerListItemProps {
  iconComponent: string;
  iconComponentAlt: string;
  label: string;
  iconStyle?: CSSProperties;
}

const ActionModalCategoryPickerListItem: React.FC<ActionModalCategoryPickerListItemProps> = ({
  iconComponent,
  iconComponentAlt,
  label,
  iconStyle,
}) => {
  const { listItem, listIcon, listItemText } = useStyles();
  const [selected, setSelected] = useState(false);
  const classes = useStyles();
  const { setCategory } = useTransactionsContext();

  const clickHandler = (newLabel: string): void => {
    setSelected(!selected);
    setCategory(newLabel);
  };

  return (
    <ListItem
      button
      className={listItem}
      onClick={(): void => clickHandler(label)}
      selected={selected}
      classes={{
        selected: classes.selected,
      }}
    >
      <ListItemIcon className={listIcon}>
        <img src={iconComponent} alt={iconComponentAlt} style={iconStyle} />
      </ListItemIcon>
      <ListItemText primary={label} classes={{ primary: listItemText }} />
    </ListItem>
  );
};

export default ActionModalCategoryPickerListItem;
