import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ActionModalButtonSave from './ActionModalButtonSave';
import ActionModalButtonCancel from './ActionModalButtonCancel';

const useStyles = makeStyles(() => ({
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
}));

interface ActionModalButtonsProps {
  id: string;
}

const ActionModalButtons: React.FC<ActionModalButtonsProps> = ({ id }) => {
  const { buttons } = useStyles();

  return (
    <div className={buttons}>
      <ActionModalButtonCancel />
      <ActionModalButtonSave id={id} />
    </div>
  );
};

export default ActionModalButtons;
