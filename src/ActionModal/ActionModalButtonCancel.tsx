import React from 'react';
import { Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { withRouter } from 'react-router-dom';
import { RouteComponentProps } from 'react-router';
import { GlobalColors } from '../globals';

const ActionModalButtonCancel: React.FC<RouteComponentProps> = ({
  history,
}) => {
  const CancelButton = withStyles(() => ({
    root: {
      color: '#7038A7',
      fontFamily: `'Jost', sans-serif`,
      fontSize: 18,
      fontWeight: 600,
      backgroundColor: GlobalColors.WHITE,
      padding: '10px 20px',
      textTransform: 'capitalize',
      borderRadius: 8,
      marginRight: 8,
      '&:hover': {
        backgroundColor: '#efefef',
      },
    },
  }))(Button);

  const clickHandler = (): void => {
    history.replace('/');
  };

  return <CancelButton onClick={clickHandler}>Cancel</CancelButton>;
};

export default withRouter(ActionModalButtonCancel);
